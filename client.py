#!/usr/bin/python

import os
import logging

from flotype.bridge import Bridge 

bridge = Bridge(log_level=logging.DEBUG, api_key='abcdefgh')

try:
    input = raw_input
except:
    pass

class MsgHandler(bridge.Service):
    def msg(self, name, message):
        print(name + ': ' + message)

def start_client():
    name = input('Name: ')
    chat = bridge.get_service('chat')
    chat.join(MsgHandler)
    pid = os.fork()
    if pid == 0:
        while True:
            chat.send(name, input('> '))
 
bridge.ready(start_client)
