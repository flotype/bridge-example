#!/usr/bin/python

import sys
from flotype.bridge import Bridge 

bridge = Bridge(host='localhost', port=8090, api_key='abcdefgh')


random = bridge.get_service('random') 
print "Got service: random"

def callback(val):
  print "Random number: " + str(val)

  
random.generate(int(sys.argv[1]), callback)
  
bridge.connect()
