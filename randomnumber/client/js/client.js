var Bridge = require('bridge');


var bridge = new Bridge({host: 'localhost', port: 8090, apiKey: 'abcdefgh'}).connect();

bridge.getService('random', function(random, name){
  console.log('Got service: ' + name);
  
  random.generate(10, function(val){
    console.log('Random number: ' + val);
  });
});
