require 'rubygems' 
require 'bridge'

EventMachine.run do
  

  bridge = Bridge::Bridge.new({:host => 'localhost', :port => 8090, :api_key => 'abcdefgh'}).connect

  bridge.get_service 'random' do |random, name|
    puts "Got service: #{name}"
    
    random.generate Integer(ARGV[0]) do |val|
      puts("Random number: #{val}");
    end
  end

end
