require 'rubygems' 
require 'bridge'

EventMachine.run do
  

  bridge = Bridge::Bridge.new({:host => 'localhost', :port => 8090, :api_key => 'abcdefgh'}).connect

  module RandomService 
    def self.generate max
      yield rand(max)
    end
  end

  bridge.publish_service 'random', RandomService do |name|
    puts "Successfully published service: #{name}"
  end

end
