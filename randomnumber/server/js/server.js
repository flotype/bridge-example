var Bridge = require('bridge');


var bridge = new Bridge({host: 'localhost', port: 8090, apiKey: 'abcdefgh'}).connect();

var randomService = {
  generate: function(max, callback) {
    callback(Math.random() * (max - 1));
  }
};

bridge.publishService('random', randomService, function(name){
  console.log('Successfully published service: ' + name);
});
