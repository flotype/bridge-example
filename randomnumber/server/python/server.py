#!/usr/bin/python

import random
from flotype.bridge import Bridge 

bridge = Bridge(host='localhost', port=8090, api_key='abcdefgh')

class RandomService(object):
  def generate(self, max, callback):
    callback(random.uniform(0, max))

    
def callback(name):
  print 'Successfully published server: ' + name
  
bridge.publish_service('random', RandomService(), callback)

bridge.connect()
