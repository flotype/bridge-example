import com.flotype.bridge.*;

public class ChatServer {
	public static void main(String[] args) {

		final Bridge bridge = new Bridge().setApiKey("abcdefgh");
		bridge.connect();
		
		Service chatService = new Service(){
			public void join (ChatServiceClient handler) {
				bridge.joinChannel("myroom", handler);
			}
			
			public void send (String name, String message) {
				ChatServiceClient myroom = bridge.getChannel("myroom", ChatServiceClient.class);
				myroom.msg(name, message);
			}
		};
		
		bridge.publishService("chat", chatService);
		
	}
	
}
