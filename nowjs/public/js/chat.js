bridge = new Bridge({apiKey: 'abcdefgh'});
bridge.connect();

var name = prompt("What's your name?", "");
$(document).ready(function () {
  var chat = {};
  chat.receiveMessage = function(name, message) {
    $("#messages").append("<br>" + name + ": " + message);
  };
  bridge.joinChannel('chat', chat, Function.prototype);
});

bridge.getService('now', function (now) {
  $("#send-button").click(function () {
    now.distributeMessage(name, $("#text-input").val());
    $("#text-input").val("");
  });
});
