bridge = new Bridge({apiKey: 'abcdefgh'});
bridge.connect(function () {
  console.log('connected!');
});

bridge.joinChannel('notifications', {
  notify: function (names) {
    var content = ''; 
    for(var i = 0; i < names.length; i++) {
      content += names[i] + ' has poked you!';
    }
    $('#notifications').html(content);
  }
});
var func;
bridge.getService('now', function (now) {
  console.log('got service!');
  $(document).ready(function () {
    $('#poke').click(func = function () {
      console.log('clicked!');
      var name = $('#name').val();
      if(name.length) {
	now.sendNotification(name);
      } else {
	alert('please enter a name!');
      }   
    }); 
  });
});
