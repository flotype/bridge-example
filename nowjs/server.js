var Bridge = require('bridge');
var bridge = new Bridge({apiKey: 'abcdefgh'});
bridge.connect(function () {
  console.log(bridge._connection.clientId);
});

var now = {};
var channelCache = {};

var lastFour = [];

var populateCache = function (channel, cb) {
  bridge.getChannel(channel, function (ch) {
    channelCache[channel] = ch;
    console.log('Cached ' + channel);
    cb();
  });
};

now.sendNotification = function (name) {
  if (lastFour.length >= 4) {
    lastFour.splice(0, lastFour.length - 3);
  }
  lastFour.push(name);
  if (channelCache.notifications === undefined) {
    populateCache('notifications', function () {
      now.sendNotification(name);
    });
  } else {
    channelCache.notifications.notify(lastFour);
  };
}

now.distributeMessage = function (name, message) {
  if (channelCache.chat === undefined) {
    populateCache('chat', function () {now.distributeMessage(name, message);});
  } else {
    channelCache.chat.receiveMessage(name, message);
  }
};

var actors = [];

now.connect_mmap = function (clientId) {
  actors[clientId] = {x: 0, y: 0};
};

now.updateActor = function (clientId, x, y) {
  actors[clientId].x = x;
  actors[clientId].y = y;

  if (channelCache.multiplayer_map === undefined) {
    populateCache('multiplayer_map', function () {now.updateActor(clientId, x, y);});
  } else {
    toUpdate = {};
    for (var i in actors) {
      if(Math.abs(x - actors[i].x) < 310 && Math.abs(y - actors[i].y) < 210) {
        toUpdate[i] = {x: actors[i].x, y: actors[i].y};
      }
    }
    channelCache.multiplayer_map.drawActors(toUpdate);
  }
}

bridge.publishService('now', now);

var express = require('express');
var app = express.createServer();
app.use(express.static(__dirname + '/public'));
app.listen(8080);
